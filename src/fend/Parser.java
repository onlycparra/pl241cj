package fend;

import java.io.FileNotFoundException;
import java.util.Stack;
import static fend.utiles.domain.TType.*;
import java.io.IOException;

/**
 *
 * @author claudio
 * EBNF for PL241
 * <pre>
 * {@code
 * letter = “a” | “b” | ... | “z”.
 * digit = “0” | “1” | ... | “9”.
 * relOp = “==“ | “!=“ | “<“ | “<=“ | “>“ | “>=“.
 *
 * ident = letter {letter | digit}.
 * number = digit {digit}.
 *
 * designator = ident{ "[" expression "]" }.
 * factor = designator | number | “(“ expression “)” | funcCall .
 * term = factor { (“*” | “/”) factor}.
 * expression = term {(“+” | “-”) term}.
 * relation = expression relOp expression .
 *
 * assignment = “let” designator “<-” expression.
 * funcCall = “call” ident [ “(“ [expression { “,” expression } ] “)” ].
 * ifStatement = “if” relation “then” statSequence [ “else” statSequence ] “fi”.
 * whileStatement = “while” relation “do” StatSequence “od”.
 * returnStatement = “return” [ expression ] .
 *
 * statement = assignment | funcCall | ifStatement | whileStatement | returnStatement.
 * statSequence = statement { “;” statement }.
 *
 * typeDecl = “var” | “array” “[“ number “]” { “[“ number “]” }.
 * varDecl = typeDecl indent { “,” ident } “;” .
 * funcDecl = (“function” | “procedure”) ident [formalParam] “;” funcBody “;” .
 * formalParam = “(“ [ident { “,” ident }] “)” .
 * funcBody = { varDecl } “{” [ statSequence ] “}”.
 *
 * computation = “main” { varDecl } { funcDecl } “{” statSequence “}” “.” .
 * }
 * </pre>
*/
public class Parser {
    private static final Stack<Parser> SCOPE_STACK;
    private final Scanner sc0;
    
    static{
        SCOPE_STACK = new Stack<>();
    }
    
    public Parser(String filename, int cursor) throws FileNotFoundException, IOException {
        sc0 = new Scanner(filename,cursor);
        SCOPE_STACK.push(this);
    }
    
    private void error(Object expected, Object received) {
        System.out.println("HORROR: I expected " + expected + ", but you gave me " + received);
        System.exit(2);
    }
    
    public Boolean parseComputation(){
        //WORK IN PROGRESS
        Boolean main,vD,fD,open,stats,close,end;
        
        main  = sc0.nextToken() == MINT; //main
        if(!main) error(MINT,sc0);
        
        vD    = parseVarDecl();
        fD    = parseFuncDecl();
        
        open  = sc0.nextToken() == LCUT; //{
        if(!open) error(LCUT,sc0);
        
        stats = parseStatSequence();
        
        close = sc0.nextToken() == RCUT; //}
        if(!close) error(RCUT,sc0);
        
        end   = sc0.nextToken() == PERT; //.
        if(!end) error(PERT,sc0);
        
        return main && vD && fD && open && stats && close && end;
    }

    private Boolean parseVarDecl() {
        Boolean tD,idn,comma,scol;
        return false;
    }

    private Boolean parseFuncDecl() {
        return false;
    }
    
    private Boolean parseStatSequence() {
        return false;
    }
    
    
    
    
    
}
