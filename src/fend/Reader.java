package fend;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reads file character by character
 * @author claudio
 */
public final class Reader {
    private final FileReader source;
    private final BufferedReader buffer;
    private final String filename;
    
    
    private int cursor; //The cursor that traverses the entire document.
    private int row; //The current line number, increments with each '\n'.
    private int lastChar; //last character read
    
    
    //col[0]=current position in the line, col[1]=previous value of col[0]
    private int[] col; 
    //cL[0]=last line of code, cL[1]=previous value of cL[0]
    private String[] codeLine; 
    
    
    public Reader(String filename) throws FileNotFoundException{
        this.source = new FileReader(filename);
        this.buffer = new BufferedReader(source);
        this.filename = filename;
        
        this.cursor = 0;
        this.row = 1;
        this.lastChar = 0;
        
        this.col = new int[] {0,0};
        this.codeLine = new String[] {"",""};
    }
    
    /** 
     * Creates a reader that start reading at the position indicated by cursor
     * @param filename
     * @param cursor
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public Reader(String filename, int cursor) throws FileNotFoundException, IOException{
        source = new FileReader(filename);
        buffer = new BufferedReader(source);
        this.filename = filename;
        row = 1;
        lastChar = 0;
        col = new int[] {0,0};
        codeLine = new String[] {"",""};
        
        
        if(cursor>0){
            this.cursor = cursor;
            for (int i = 0; i < cursor; i++) {
                nextChar();
                if(lastChar == -1) break;
            }
        }else{
            this.cursor = 0;
        }
    }

    /**
     * Marks buffer, gets a new character, advance to the next position
     * and returns the character.
     * @return the new character seen
     */
    public int nextChar(){
        try {
            buffer.mark(1);//marks buffer for a possible "restoreChar()"
            lastChar = buffer.read();
            cursor += 1;
            switch (lastChar) {
                case '\n':
                    codeLine[1] = codeLine[0];
                    codeLine[0] = "";
                    col[1] = col[0];
                    col[0] = 0;
                    row += 1;
                    break;
                case '\r':
                    break;
                default:
                    codeLine[0] += (char)lastChar;
                    col[0] += 1;
                    break;
            }
        }catch (IOException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR in Reader.nextChar(),Cannot read from " + filename + ".");
            System.exit(-1);
        }
        return lastChar;
    }
    
    /**
     * Try to go one position back in the buffer, only if nextChar()
     * has been performed before.
     */
    public void restoreChar(){
        try {
            buffer.reset();
            cursor -= 1;
            switch (lastChar) {
                case '\n':
                    codeLine[0] = codeLine[1];
                    codeLine[1] = "";
                    col[0] = col[1];
                    col[1] = 0;
                    row -= 1;
                    break;
                case '\r':
                    break;
                default:
                    codeLine[0] = codeLine[0].substring(0, codeLine[0].length()-2);
                    col[0] -= 1;
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR in Reader.restoreChar(), cannot call more than once.");
            System.exit(-1);
        }
    }
    
    public int getCursor(){
        return cursor;
    }

    public int getCol() {
        return col[0];
    }
    
    public int getRow() {
        return row;
    }
    public String getFilename() {
        return filename;
    }
    
    public String getCodeLine(){
        return codeLine[0];
    }
    
    public String getCodeCol(){
        String secondLine = "";
        for (int i = 0; i < codeLine[0].length()-1; i++) {
            secondLine +=' ';
            
        }
        secondLine += "^\n";
        return codeLine[0] + "/n" + secondLine;
    }
}
