package fend.utiles.domain;

import fend.utiles.general.Triplet;
import java.util.ArrayList;

/**
 * Stack containing the variables already found and their last incarnation.
 * each element in the stack has a identifier name, the operation that defines
 * that identifier and the scope of that identifier 
 * @author claudio
 */
public class IdentStack{
    private ArrayList<Triplet<String,Operator,Integer>> identStack;
    private int scopeLevel;

    public IdentStack() {
        identStack = new ArrayList<>();
        scopeLevel = 0;
    }
    
    public void push(String name, Operator op){
        Triplet<String,Operator,Integer> t0 = new Triplet<>(name,op,scopeLevel);
        identStack.add(0, t0);
    }
    public Operator search(String name){
        Operator op = new Operator();
        return op;
    }
    public void openScope(){
        
    }
    public void closeScope(){
        
    }
    
}
