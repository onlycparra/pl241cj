package fend.utiles.domain;

import fend.utiles.general.DoubleKeyHashMap;

/**
 * Types of Tokens, contains all the definitions (string,id) of the language
 * @author claudio
 */

public enum TType {
    //<editor-fold defaultstate="collapsed" desc="Definitions">

    /**
     * error token type
     */
    ERRT("\\err", 0),
    /**
     * *
     */
    TIMT("*", 1),
    /**
     * /
     */
    DIVT("/", 2),
    /**
     * +
     */
    PLUT("+", 11),
    /**
     * -
     */
    MINT("-", 12),
    /**
     * ==
     */
    EQLT("==", 20),
    /**
     * !=
     */
    NEQT("!=", 21),
    /**
     * &lt
     */
    LSST("<", 22),
    /**
     * >=
     */
    GEQT(">=", 23),
    /**
     * &lt=
     */
    LEQT("<=", 24),
    /**
     * >
     */
    GRET(">", 25),
    /**
     * .
     */
    PERT(".", 30),
    /**
     * ,
     */
    COMT(",", 31),
    /**
     * [
     */
    LBRT("[", 32),
    /**
     * ]
     */
    RBRT("]", 34),
    /**
     * )
     */
    RPRT(")", 35),
    /**
     * {@literal <-}
     */
    BECT("<-", 40),
    /**
     * then
     */
    THET("then", 41),
    /**
     * do
     */
    DOT("do", 42),
    /**
     * (
     */
    LPRT("(", 50),
    /**
     * number token type
     */
    NUMT("\\num", 60),
    /**
     * identifier token type
     */
    IDEN("\\ide", 61),
    /**
     * ;
     */
    SCOT(";", 70),
    /**
     * }
     */
    RCUT("}", 80),
    /**
     * od
     */
    ODT("od", 81),
    /**
     * fi
     */
    FIT("fi", 82),
    /**
     * else
     */
    ELST("else", 90),
    /**
     * let
     */
    LETT("let", 100),
    /**
     * call
     */
    CALT("call", 101),
    /**
     * if
     */
    IFT("if", 102),
    /**
     * while
     */
    WHLT("while", 103),
    /**
     * return
     */
    RETT("return", 104),
    /**
     * "var" reserved word
     */
    VART("var", 110),
    /**
     * array
     */
    ARRT("array", 111),
    /**
     * function
     */
    FUNT("function", 112),
    /**
     * procedure
     */
    PROT("procedure", 113),
    /**
     * {
     */
    LCUT("{", 150),
    /**
     * main
     */
    MAIT("main", 200),
    /**
     * // comment token
     */
    CMTT("//", 253),
    /**
     * # comment token
     */
    CM2T("#", 254),
    /**
     * end of file token type
     */
    EOFT("\\eof", 255);
//</editor-fold>//</editor-fold>

    
    private static final DoubleKeyHashMap<String, Integer, TType> TOKENS_TABLE;
    //<editor-fold defaultstate="collapsed" desc="TOKENS_TABLE filling">
    static{
        TOKENS_TABLE = new DoubleKeyHashMap<>(80,0.5f);
        TOKENS_TABLE.put("\\err",	0,	ERRT);
        TOKENS_TABLE.put("*",		1,	TIMT);
        TOKENS_TABLE.put("/",		2,	DIVT);
        TOKENS_TABLE.put("+",		11,	PLUT);
        TOKENS_TABLE.put("-",		12,	MINT);
        TOKENS_TABLE.put("==",		20,	EQLT);
        TOKENS_TABLE.put("!=",		21,	NEQT);
        TOKENS_TABLE.put("<",		22,	LSST);
        TOKENS_TABLE.put(">=",		23,	GEQT);
        TOKENS_TABLE.put("<=",		24,	LEQT);
        TOKENS_TABLE.put(">",		25,	GRET);
        TOKENS_TABLE.put(".",		30,	PERT);
        TOKENS_TABLE.put(",",		31,	COMT);
        TOKENS_TABLE.put("[",		32,	LBRT);
        TOKENS_TABLE.put("]",		34,	RBRT);
        TOKENS_TABLE.put(")",		35,	RPRT);
        TOKENS_TABLE.put("<-",		40,	BECT);
        TOKENS_TABLE.put("then",	41,	THET);
        TOKENS_TABLE.put("do",		42,	DOT );
        TOKENS_TABLE.put("(",		50,	LPRT);
        TOKENS_TABLE.put("\\num",	60,	NUMT);
        TOKENS_TABLE.put("\\ide",	61,	IDEN);
        TOKENS_TABLE.put(";",		70,	SCOT);
        TOKENS_TABLE.put("}",		80,	RCUT);
        TOKENS_TABLE.put("od",		81,	ODT );
        TOKENS_TABLE.put("fi",		82,	FIT );
        TOKENS_TABLE.put("else",	90,	ELST);
        TOKENS_TABLE.put("let",    	100,    LETT);
        TOKENS_TABLE.put("call",	101,	CALT);
        TOKENS_TABLE.put("if",		102,	IFT );
        TOKENS_TABLE.put("while",	103,	WHLT);
        TOKENS_TABLE.put("return",	104,	RETT);
        TOKENS_TABLE.put("var",    	110,    VART);
        TOKENS_TABLE.put("array",	111,	ARRT);
        TOKENS_TABLE.put("function",    112,	FUNT);
        TOKENS_TABLE.put("procedure",   113,	PROT);
        TOKENS_TABLE.put("{",		150,	LCUT);
        TOKENS_TABLE.put("main",	200,	MAIT);
        TOKENS_TABLE.put("//",          253,	CMTT);
        TOKENS_TABLE.put("#",           254,	CM2T);
        TOKENS_TABLE.put("\\eof",	255,	EOFT);
    }
//</editor-fold>
    
    /**
     * Given an Id, get the token type of that Id.
     *
     * @param id: Id to lookup
     * @return if the ID exists, returns its token, otherwise null.
     */
    public static TType idToTType(int id) {
        return TOKENS_TABLE.getByK2(id);
    }
    /**
     * Given a token name (string), get the token type for that name.
     *
     * @param name: token name to lookup
     * @return if the name exists, returns its token, otherwise null.
     */
    public static TType nameToTType(String name) {
        return TOKENS_TABLE.getByK1(name);
    }


    private final String name;
    private final int id;
    
    private TType(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public Boolean isErr() {
        return (id == 0);
    }
    public Boolean isReserved() {
        return (id != 0 &&
                id != 60 &&
                id != 61 &&
                id != 255);
    }
    public Boolean isNum() {
        return (id == 60);
    }
    public Boolean isIdent() {
        return (id == 61);
    }
    /**
     * Is comment?
     * @return
     */
    public Boolean isCmt() {
        return (id == 253 ||
                id == 254);
    }
    public Boolean isEof() {
        return (id == 255);
    }

    /**
     * get the name of this token. (like "<b>main</b>", "<b>{</b>",
     * "<b>\num</b>" , "<b>\&lt-</b>").
     */
    public String getName() {
        return name;
    }
    /**
     * get the ID of this token. (like "<b>200</b>" for "main", "<b>255</b>" for
     * EOF, "<b>61</b>" for identifiers , "<b>102</b>" for "if").
     */
    public int getId() {
        return id;
    }
    
    
    public String toString(){
        String nam = "";
        
        if(isErr()){
            nam = "ERROR";
        }else if(isReserved()){
            nam = getName();
        }else if (isNum()) {
            nam = "NUMBER";
        }else if (isIdent()) {
            nam = "IDENTIFIER";
        }else if (isEof()){
            nam = "EOF";
        }
        return nam;
    }
}
