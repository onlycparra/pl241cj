package fend.utiles.general;

import fend.utiles.general.Pair;
import java.util.HashMap;

/**
 * Hash Table with double a, Value can be recovered with any of them.
 * @author claudio
 * @param <K1> First Key
 * @param <K2> Second Key
 * @param <V>  Value to store
 */
public class DoubleKeyHashMap<K1,K2,V> {
    private final HashMap<K1,Pair<K2,V>> table1;
    private final HashMap<K2,Pair<K1,V>> table2;

    public DoubleKeyHashMap(int initialCapacity, float loadFactor) {
        table1 = new HashMap<>(initialCapacity, loadFactor);
        table2 = new HashMap<>(initialCapacity, loadFactor);
    }
    
    public V put(K1 key1, K2 key2, V value){
        
        V oldValue = removeByK1(key1);
        removeByK2(key2);
        
        Pair<K2,V> p1 = new Pair<>(key2, value);
        Pair<K1,V> p2 = new Pair<>(key1, value);
        
        table1.put(key1, p1);
        table2.put(key2, p2);
        
        return oldValue;
    }
    
    public V removeByK1(K1 firstKey){
        Pair<K2,V> p0 = table1.get(firstKey);
        table1.remove(firstKey);
        V ret = null;
        if(p0!=null){
            K2 k2 = p0.a;
            table2.remove(k2);
            ret = p0.b;
        }
        return ret;
    }
    public V removeByK2(K2 secondKey){
        Pair<K1,V> p0 = table2.get(secondKey);
        table2.remove(secondKey);
        
        V ret = null;
        if(p0!=null){
            K1 k1 = p0.a;
            table1.remove(k1);
            ret = p0.b;
        }
        return ret;   
    }
    
    /**
     * Gets the b to which the specified a K1 is mapped, or null if
 there is no mapping for that a 
     * @return
     */
    public V getByK1(K1 key1){
        V ret;
        Pair<K2,V> p1 = table1.get(key1);
        if(p1 != null){
            ret = p1.b;
        }else{
            ret = null;
        }
        
        return ret;
    }
    /**
     * Gets the b to which the specified a K2 is mapped, or null if
 there is no mapping for that a 
     * @return
     */
    public V getByK2(K2 key2){
        
        V ret;
        Pair<K1,V> p1 = table2.get(key2);
        if(p1 != null){
            ret = p1.b;
        }else{
            ret = null;
        }
        
        return ret;
    }
}
