package fend.utiles.general;

import java.util.HashMap;

/**
 * Hash Table bidirectional, every pair (K1,K2) is unique,
 can getByK1 and removeByK1 trough K1 or K2.
 * @author claudio
 * @param <K1> Key
 * @param <K2> Value (Second Key)
 */
public class BidirHashMap<K1,K2> {
    private final HashMap<K1,K2> table1;
    private final HashMap<K2,K1> table2;

    public BidirHashMap(int initialCapacity, float loadFactor) {
        table1 = new HashMap<>(initialCapacity, loadFactor);
        table2 = new HashMap<>(initialCapacity, loadFactor);
    }
    
    public K2 put(K1 key, K2 value){
        K2 oldValue = removeByK1(key);
        removeByK2(value);

        table1.put(key, value);
        table2.put(value, key);
        
        return oldValue;
    }
    
    public K2 removeByK1(K1 key){
        K2 oldValue = table1.get(key);
        
        table1.remove(key);
        table2.remove(oldValue);
        
        return oldValue;
    }
    public K1 removeByK2(K2 value){
        K1 oldKey = table2.get(value);
        
        table2.remove(value);
        table1.remove(oldKey);
        
        return oldKey;
    }
    
    /**
     * Gets the value to which the K1 key is mapped, or null if there is no
     * mapping for that key 
     * @param key
     * @return
     */
    public K2 getByK1(K1 key){
        return table1.get(key);
    }
    /**
     * Gets the value to which the K2 key is mapped, or null if there is no
     * mapping for that key 
     * @param key
     * @return
     */
    public K1 getByK2(K2 value){
        return table2.get(value);
    }
}
