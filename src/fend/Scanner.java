package fend;

import fend.utiles.domain.TType;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Scans file word by word identifying each token type according with the
 * programming language definitions
 * @author claudio
 */
public class Scanner {
    private final Reader reader;
    
    
    /**
     * Last token obtained from the Scanner, nextToken() updates this field.
     */
    private TType lastTType;
    private Integer lastNum;
    private String lastIdent;
    private String lastError;
    
    /**
     * flag to indicate that getToken() just needs to return the previously
     * obtained token. restoreToken() makes it true, and getToken() makes it false
     */
    private Boolean wentBack;
    
    /**
     * Test Scanner in a given filename writing in output the result of the
     * process.
     * @param filename: file containing the source codes
     * @param cursor: starting position
     * @throws java.io.IOException
     */
    public static void selfTest(String filename, int cursor) throws IOException{
        System.out.println("\n\n────────────────────────────────────────────");
        System.out.println("Self test for: " + filename);
        try{
            Scanner sc0 = new Scanner(filename,cursor);
            String line, code, name, content = "";
            Integer currLine = 1 ;
            TType type;
            
            for (int i = 0; i < 2147483647; ++i) {
                type = sc0.nextToken();
                
                //print vertical space if jumping to the next line
                if(currLine < sc0.getCurrentRow()){
                    System.out.println();
                    currLine = sc0.getCurrentRow();
                }
                
                //print token info
                line = String.format("%4s",("L" + sc0.getCurrentRow()));
                if(type != null){
                    code = String.format("%3d",type.getId());
                    name = String.format("%6s", type);
                    switch (type) {
                        case ERRT:
                            content = sc0.lastError;
                            break;
                        case NUMT:
                            content = Integer.toString(sc0.lastNum);
                            break;
                        case IDEN:
                            content = sc0.lastIdent;
                            break;
                        default:
                            content = "";
                            break;
                    }
                    System.out.println(line + " " + code + " " + name + " " + content);

                    if(type.isEof()) break;
                }else{
                    break;
                }
                
                
            }
        }catch(FileNotFoundException e){
            System.out.println("Couldn't open " + filename);
        }
    }

    public Scanner(String filename,int cursor) throws FileNotFoundException, IOException{
        reader = new Reader(filename,cursor);
        lastTType = null;
        lastNum = null;
        lastIdent = null;
        lastError = null;
        wentBack = false;
        
    }
    
    /**
     * Extracts and returns a token type from the source file.
     * @return
     */
    public TType nextToken(){
        boolean commentsSkiped = false;
        if(!wentBack){
            int lastChar = reader.nextChar(); //fetch a character form reader

            //skip spaces
            while(
            lastChar == ' '  ||
            lastChar == '\r' ||
            lastChar == '\n' || 
            lastChar == '\t'
            ){
                lastChar = reader.nextChar();
            }
            
            
            switch(lastChar){
                case -1:{
                    lastTType = TType.EOFT;
                    break;
                }
                case '*':case '+':case '-':case '.':case ',':case '[':
                case ']':case ')':case '(':case ';':case '}':case '{':
                    lastTType = TType.nameToTType(Character.toString((char)lastChar));
                    break;
                case '#':
                    //this is a comment, skip it
                    do{
                        lastChar = reader.nextChar();
                    }while(lastChar != '\r' && lastChar != '\n' && lastChar != -1);
                    commentsSkiped = true;
                    break;
                case '/':
                    lastChar = reader.nextChar();
                    if(lastChar != '/'){
                        //it was only one '/'
                        lastTType = TType.DIVT;
                        reader.restoreChar();
                    }else{
                        //this is a comment, skip it
                        do{
                            lastChar = reader.nextChar();
                        }while(lastChar != '\r' && lastChar != '\n' && lastChar != -1);
                        commentsSkiped = true;
                    }
                    break;
                case '=':
                    lastChar = reader.nextChar();
                    if(lastChar == '='){
                        lastTType = TType.EQLT;
                    }else{
                        // 0 (error) because '=' can only be consecuted by '=' to form "=="
                        String msg;
                        if (lastChar == '\r' || lastChar == '\n' || lastChar == -1) {
                            msg = "\\n";
                        } else {
                            msg = Character.toString((char) lastChar);
                        }
                        lastError = "=" + msg;
                        lastTType = TType.ERRT;
                        reader.restoreChar();
                    }
                    break;

                case '!':
                    lastChar = reader.nextChar();
                    if(lastChar == '='){
                        lastTType = TType.NEQT;
                    }else{
                        // 0 (error) because '!' can only be consecuted by '=' to form "!="
                        String msg;
                        if (lastChar == '\r' || lastChar == '\n' || lastChar == -1) {
                            msg = "\\n";
                        } else {
                            msg = Character.toString((char) lastChar);
                        }
                        lastError = "!" + msg;
                        lastTType = TType.ERRT;
                        reader.restoreChar();
                    }
                    break;

                case '<':
                    lastChar = reader.nextChar();
                    switch(lastChar){
                        case '=':
                            lastTType = TType.LEQT;
                            break;
                        case '-':
                            lastTType = TType.BECT;
                            break;
                        default:
                            //it was only '<', so the next character is restored
                            lastTType = TType.LSST;
                            reader.restoreChar();
                    }
                    break;

                case '>':
                    lastChar = reader.nextChar();
                    switch(lastChar){
                        case '=':
                            lastTType = TType.GEQT;
                            break;
                        default:
                            //it was only '>', so the next character is restored
                            lastTType = TType.GRET;
                            reader.restoreChar();
                    }
                    break;

                

                //positive numbers
                case 48: case 49: case 50: case 51: case 52:
                case 53: case 54: case 55: case 56: case 57:
                    int num = 0;
                    for(int i=0; i<21; ++i){
                        if(i==20){
                            System.out.println("WARNING in Scanner.getToken(): "
                                    + "number too big (more than 19 digits)");
                        }
                        if(47<lastChar && lastChar<58){
                            num = 10*num + lastChar-48;
                            lastChar = reader.nextChar();
                        }else{
                            reader.restoreChar();
                            break;
                        }
                    }
                    lastTType = TType.NUMT;
                    lastNum = num;
                    break;

                //identifiers or reserved-words
                default:
                    //all identifiers start with a letter
                    if(Character.isLetter((char)lastChar)){
                    
                        //obtain the word itself
                        String word = "";
                        for(int i=0; i<100; ++i){
                            if(i==99){
                                System.out.println("WARNING in Scanner.getToken(): "
                                        + "identifier too big (more than 99 characters)");
                            }
                            if(Character.isLetterOrDigit((char)lastChar)){
                                word = word + ((char)lastChar);
                                lastChar = reader.nextChar();
                            }else{
                                reader.restoreChar();
                                break;
                            }
                        }
                        
                        //determining if the word is a reserved-word or an identifier
                        TType tt0 = TType.nameToTType(word);
                        
                        if(tt0 == null){ //it is an identifier
                            tt0 = TType.IDEN;
                            lastIdent = word;
                        }
                        lastTType = tt0;
                    }else{//identifier do not start with a letter
                        // error, no identifier start with something else than letters
                        lastError = Character.toString((char)lastChar);
                        lastTType = TType.ERRT;
                    }
                //end default

            } //end switch
            if(commentsSkiped){
                return nextToken();
            }
        }//end if(wentBack)
        wentBack = false;
        return lastTType;
    }
    public void restoreToken(){
        if(wentBack){
            System.out.println("ERROR in Scanner.goBack(), cannot call more than once.");
            System.exit(-1);
        }else{
            wentBack = true;
        }
        
    }
    
    /**
     * Returns the last token type obtained, does not advance to the next in the
     * document, it just peek it
     * @return 
     */
    public TType getLastToken() {
        return lastTType;
    }
    /**
     * Returns the last number obtained.
     * @return 
     */
    public Integer getLastNum() {
        return lastNum;
    }
    /**
     * Returns the last identifier obtained.
     * @return 
     */
    public String getLastIdent() {
        return lastIdent;
    }

    public int getCurrenrCursor() {
        return reader.getCursor();
    }
    public int getCurrentRow() {
        return reader.getRow();
    }
    public int getCurrentCol(){
        return reader.getCol();
    }
    
    @Override
    public String toString(){
        String name = "";
        if(lastTType.isErr()){
            name = " \"" + lastError + "\"";
        }else if(lastTType.isReserved()){
            name = " \"" + lastTType.getName() + "\"";
        }else if (lastTType.isNum()) {
            name = Integer.toString(lastNum);
        }else if (lastTType.isIdent()) {
            name = " \"" + lastIdent + "\"";
        }else if (lastTType.isEof()){
            name = "EOF";
        }
        return name;
    }
}
