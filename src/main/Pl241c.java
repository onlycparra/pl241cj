/*
 * 
 */
package main;

import fend.Scanner;
import java.io.IOException;

public class Pl241c {

    /**
     * @param args pl241 source code file to be compiled.
     */
    public static void main(String[] args) throws IOException {
        
        System.out.println("\n─────────── PL241 compiler (in glorious Java) ───────────");
        
        
        //checking arguments
        if(args.length == 0){
            System.out.println("So... what am I supossed to compile?\n"
                    + "Usage: java Pl241c <sourcefile>");
            System.exit(1);
        }else if(1 < args.length){
            System.out.println("I'm not that cool, I only can compile one source code at the time.\n"
                    + "Usage: java Pl241c sourcefile.pl241");
            System.exit(1);
        }
        System.out.println("compiling " + args[0] + "\n");
        
        //testing scanner
        for (int i = 0; i < 1; i++) {
            Scanner.selfTest("./inputs/test0" + String.format("%02d", i+1) + ".txt",0);
        }
        
        
        
    }
    
}
